import java.util.Scanner;

public class RotateImage{
   public static void main(String[] args)
   {
      //Get input from user to either make an automatic or custom array
      Scanner input = new Scanner(System.in);
      int[][] values = null;
      System.out.println("\tRotate Image");
      char choice;
      do
      {
         System.out.println("(a)Get a prefilled array (c)Custom Array (q)Quit");
         choice = input.next().charAt(0);
         
         switch(choice)
         {
            case 'a':
               System.out.println("Enter size of array e.g 5:");
               values = generateAutoArray(input.nextInt());
               System.out.println("Array made is:");
               print(values);
               System.out.println();
               System.out.println();
               break;
               
            case 'c':
               System.out.println("Enter value for rows and columns:");
               int size = input.nextInt();
               values = new int[size][size];
         
               for(int i = 0; i < size; i++)
               {
                  System.out.println("Enter row "+(i+1)+" values in the fomat a,b,c,...:");
                  String[] vals = input.next().split(",");
         
                  for(int j = 0; j < size; j++)
                  {
                     values[i][j] = Integer.parseInt(vals[j]);
                  }
               }
               break;
           default:
               System.out.println("Input not recognised");   
    
      }
      
      //print rotated array
      if(values != null)
      {
         System.out.println("Rotated array is");
         rotate(values);
         print(values);   
      }
    }while(choice != 'q');   
   }
   
   //output an array in a formated form
   public static void print(int[][] values)
   {
      for(int[] row : values)
         {
            for(int col : row)
            {
               System.out.printf("%5d ",col);
            }
            System.out.println();
         }
   }
   
   //rotate the image using a O(1) algorithm
   public static void rotate(int[][] values)
   {
      swap(values,0,values.length-1);
   }

   
   public static void swap(int[][] values,int start,int end)
   {
      int temp;
      if(end > 0 && start < values.length)
      {

            if(end - start == 1)
            {
               temp = values[start][start];
   
               values[start][start] = values[end][start];
               values[end][start] = values[end][end];
               values[end][end] = values[start][end];
               values[start][end] = temp;
            }
            else if(end - start == 2)
            {
               loop(values,start,end,2);
            }
            else
            {
               loop(values,start,end,values.length-1);
               swap(values,start +1, end - 1);
            }
        }
   }

   //repeat a procedure loops times
   public static void loop(int[][] values,int start,int end,int loops)
   {   
      for(int i = 0; i < loops; i++)
      {
         int temp = values[start][start];
         for(int j = start; j < end; j++)
         {
            values[j][start] = values[j+1][start];
         }
         
         for(int j = start; j < end; j++)
         {
            values[end][j] = values[end][j+1];
         }
         
         for(int j = end; j > start; j--)
         {
            values[j][end] = values[j-1][end];
         }
         
         for(int j = end; j > start+1; j--)
         {
            values[start][j] = values[start][j-1];
         }
         
         values[start][start+1] = temp;
      }
   }
   
   //create an automatic array
   public static int[][] generateAutoArray(int size)
   {
      int[][] array = new int[size][size];
      
      for(int i = 0,pos = 1; i < size; i++)
      {
         for(int j = 0; j < size; j++)
         {
            array[i][j] = pos++;
         }
      }
      
      return array;
   }
}